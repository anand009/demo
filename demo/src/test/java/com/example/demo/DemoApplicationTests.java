package com.example.demo;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DemoApplicationTests {
	
	@Test
	public void test() {
		Add ob = new Add();
		assertEquals(5, ob.sum(2, 3));
		assertEquals(4, ob.sum(1, 3));
	}

}
